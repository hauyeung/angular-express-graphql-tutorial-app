export const environment = {
  production: true,
  graphqlUrl: 'http://localhost:3000/graphql',
  photosUrl: 'http://localhost:3000/photos'
};
