const SET_PHOTOS = 'SET_PHOTOS';

function photosReducer(state, action) {
    switch (action.type) {
        case SET_PHOTOS:
            state = action.payload;
            return state;
        default:
            return state
    }
}

export { photosReducer, SET_PHOTOS };