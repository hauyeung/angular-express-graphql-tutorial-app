import { menuReducer } from './menu-reducer';
import { photosReducer } from './photos-reducer';

export const reducers = {
  menu: menuReducer,
  photos: photosReducer
};
