import { Component, OnInit, ViewChild } from '@angular/core';
import { PhotoService } from '../photo.service';
import { environment } from 'src/environments/environment';
import { MatDialog } from '@angular/material';
import { EditPhotoDialogComponent } from '../edit-photo-dialog/edit-photo-dialog.component';
import { Store, select } from '@ngrx/store';
import { SET_PHOTOS } from '../reducers/photos-reducer';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-upload-page',
  templateUrl: './upload-page.component.html',
  styleUrls: ['./upload-page.component.scss']
})
export class UploadPageComponent implements OnInit {
  photoData: any = <any>{};
  photoArrayData: any[] = [];
  page: number = 1;
  totalPhotos: number = 0;
  @ViewChild('photoUpload', null) photoUpload: any;
  displayedColumns: string[] = [
    'photoUrl',
    'description',
    'tags',
    'edit',
    'delete'
  ]

  constructor(
    private photoService: PhotoService,
    public dialog: MatDialog,
    private store: Store<any>
  ) {
    store.pipe(select('photos'))
      .subscribe(photos => {
        this.photoArrayData = photos;
      })
  }

  ngOnInit() {
    this.getPhotos();
  }

  clickUpload() {
    this.photoUpload.nativeElement.click();
  }

  handleFileInput(files) {
    console.log(files);
    this.photoData.file = files[0];
  }

  save(uploadForm: NgForm) {
    if (uploadForm.invalid || !this.photoData.file) {
      return;
    }
    const {
      file,
      description,
      tags
    } = this.photoData;
    this.photoService.addPhoto(file, description, tags)
      .subscribe(res => {
        this.getPhotos();
      })
  }

  getPhotos() {
    this.photoService.getPhotos(this.page)
      .subscribe(res => {
        const photoArrayData = (res as any).data.getPhotos.photos.map(p => {
          const { id, description, tags } = p;
          const pathParts = p.fileLocation.split('/');
          const photoPath = pathParts[pathParts.length - 1];
          return {
            id,
            description,
            tags,
            photoUrl: `${environment.photosUrl}/${photoPath}`
          }
        });
        this.page = (res as any).data.getPhotos.page;
        this.totalPhotos = (res as any).data.getPhotos.totalPhotos;
        this.store.dispatch({ type: SET_PHOTOS, payload: photoArrayData });
      })
  }

  openEditDialog(index: number) {
    const dialogRef = this.dialog.open(EditPhotoDialogComponent, {
      width: '70vw',
      data: this.photoArrayData[index] || {}
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  deletePhoto(index: number) {
    const { id } = this.photoArrayData[index];
    this.photoService.deletePhoto(id)
      .subscribe(res => {
        this.getPhotos();
      })
  }
}
