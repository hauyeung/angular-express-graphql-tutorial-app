import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  constructor(
    private apollo: Apollo
  ) { }

  addPhoto(file: File, description: string, tags: string) {
    const addPhoto = gql`
      mutation addPhoto(
        $file: Upload!,
        $description: String,
        $tags: String
      ){
        addPhoto(
          file: $file,
          description: $description,
          tags: $tags
        ) {
          id,
          fileLocation,
          description,
          tags
        }
      }
    `;
    return this.apollo.mutate({
      mutation: addPhoto,
      variables: {
        file,
        description,
        tags
      },
      context: {
        useMultipart: true
      }
    })
  }

  editPhoto(id: number, file: File, description: string, tags: string) {
    const editPhoto = gql`
      mutation editPhoto(
        $id: Int!,
        $file: Upload!,
        $description: String,
        $tags: String
      ){
        editPhoto(
          id: $id,
          file: $file,
          description: $description,
          tags: $tags
        ) {
          id,
          fileLocation,
          description,
          tags
        }
      }
    `;
    return this.apollo.mutate({
      mutation: editPhoto,
      variables: {
        id,
        file,
        description,
        tags
      },
      context: {
        useMultipart: true
      }
    })
  }

  getPhotos(page: number = 1) {
    const getPhotos = gql`
      query getPhotos(
        $page: Int,
      ){
        getPhotos(
          page: $page
        ) {
          photos {
            id,
            fileLocation,
            description,
            tags
          },
          page,
          totalPhotos
        }
      }
    `;
    return this.apollo.mutate({
      mutation: getPhotos,
      variables: {
        page,
      }
    })
  }

  deletePhoto(id: number) {
    const deletePhoto = gql`
      mutation deletePhoto(
        $id: Int,
      ){
        deletePhoto(
          id: $id
        )
      }
    `;
    return this.apollo.mutate({
      mutation: deletePhoto,
      variables: {
        id,
      }
    })
  }

  searchPhotos(searchQuery: string) {
    const getPhotos = gql`
      query searchPhotos(
        $searchQuery: String,
      ){
        searchPhotos(
          searchQuery: $searchQuery
        ) {
          photos {
            id,
            fileLocation,
            description,
            tags
          },
          page,
          totalPhotos
        }
      }
    `;
    return this.apollo.mutate({
      mutation: getPhotos,
      variables: {
        searchQuery,
      }
    })
  }
}
