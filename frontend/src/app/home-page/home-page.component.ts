import { Component, OnInit, ViewChild } from '@angular/core';
import { PhotoService } from '../photo.service';
import { environment } from 'src/environments/environment';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  photoUrls: string[] = [];
  query: any = <any>{};

  constructor(
    private photoService: PhotoService
  ) { }

  ngOnInit() {
    this.getPhotos();
  }

  getPhotos() {
    this.photoService.getPhotos()
      .subscribe(res => {
        this.photoUrls = (res as any).data.getPhotos.photos.map(p => {
          const pathParts = p.fileLocation.split('/');
          const photoPath = pathParts[pathParts.length - 1];
          return `${environment.photosUrl}/${photoPath}`;
        });
      })
  }

  searchPhotos(searchForm: NgForm) {
    if (searchForm.invalid) {
      return;
    }
    this.searchPhotosQuery();
  }

  searchPhotosQuery() {
    this.photoService.searchPhotos(this.query.search)
      .subscribe(res => {
        this.photoUrls = (res as any).data.searchPhotos.photos.map(p => {
          const pathParts = p.fileLocation.split('/');
          const photoPath = pathParts[pathParts.length - 1];
          return `${environment.photosUrl}/${photoPath}`;
        });
      })
  }
}
