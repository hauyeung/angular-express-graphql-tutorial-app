import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PhotoService } from '../photo.service';
import { environment } from 'src/environments/environment';
import { Store, select } from '@ngrx/store';
import { SET_PHOTOS } from '../reducers/photos-reducer';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-edit-photo-dialog',
  templateUrl: './edit-photo-dialog.component.html',
  styleUrls: ['./edit-photo-dialog.component.scss']
})
export class EditPhotoDialogComponent implements OnInit {
  @ViewChild('photoUpload', null) photoUpload: any;
  photoArrayData: any[] = [];

  constructor(
    public dialogRef: MatDialogRef<EditPhotoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public photoData: any,
    private photoService: PhotoService,
    private store: Store<any>
  ) {
    store.pipe(select('photos'))
      .subscribe(photos => {
        this.photoArrayData = photos;
      })
  }

  ngOnInit() {
  }

  clickUpload() {
    this.photoUpload.nativeElement.click();
  }

  handleFileInput(files) {
    console.log(files);
    this.photoData.file = files[0];
  }

  save(uploadForm: NgForm) {
    if (uploadForm.invalid || !this.photoData.file) {
      return;
    }
    const {
      id,
      file,
      description,
      tags
    } = this.photoData;
    this.photoService.editPhoto(id, file, description, tags)
      .subscribe(es => {
        this.getPhotos();
      })
  }

  getPhotos() {
    this.photoService.getPhotos()
      .subscribe(res => {
        const photoArrayData = (res as any).data.getPhotos.photos.map(p => {
          const { id, description, tags } = p;
          const pathParts = p.fileLocation.split('/');
          const photoPath = pathParts[pathParts.length - 1];
          return {
            id,
            description,
            tags,
            photoUrl: `${environment.photosUrl}/${photoPath}`
          }
        });
        this.store.dispatch({ type: SET_PHOTOS, payload: photoArrayData });
        this.dialogRef.close()
      })
  }
}
